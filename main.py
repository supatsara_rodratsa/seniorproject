from flask import Flask, render_template, request
from werkzeug.utils import secure_filename
import plotly.graph_objs as go
import plotly.figure_factory as ff
import plotly
import json
import cv2
import numpy as np
import pydicom as dicom
from PIL import Image
from skimage import measure
import scipy
from skimage.morphology import convex_hull_image
import io
import base64
from keras.losses import binary_crossentropy
from keras.models import load_model
from keras.optimizers import SGD
from mrcnn.model import MaskRCNN
from mrcnn.config import Config
from keras import backend as K

app = Flask(__name__)

# var graphs = {{graph | safe}};
#                             Plotly.plot('graph', graphs, {} );
#                             window.addEventListener('resize', function(){Plotly.Plots.resize('graph');});
@app.route('/')
def homepage():
    tn, fp, fn, tp = 440, 77, 17, 506

    z = [[fn, tp],
         [tn, fp]]
    x = ['Non-Nodule', 'Nodule']
    y = ['Nodule', 'Non-Nodule']
    z_text = [['FN<br>'+str(fn),  'TP<br>'+str(tp)],
              ['TN<br>'+str(tn), 'FP<br>'+str(fp)]]
    fig = ff.create_annotated_heatmap(z, x=x, y=y,  annotation_text=z_text, colorscale=['#edf6ff', '#293a4b'])
    fig.layout.update({'height': 400, 'margin': {'l': 100, 'r': 10, 't': 50, 'b': 80}, 'paper_bgcolor': '#ffffff'})
    fig.layout.yaxis.update({'title': 'Actual Class'})
    fig.layout.xaxis.update({'title': 'Predicted Class'})


    graphJSON = json.dumps(fig, cls=plotly.utils.PlotlyJSONEncoder)

    return render_template("index.html", graph=graphJSON)


@app.route("/classification")
def classification():
    return render_template("classifyPage.html")


@app.route("/noduledetection")
def detection():
    return render_template("detectionPage.html")


@app.route("/ClassificationResult", methods=['GET', 'POST'])
def classify():
    if request.method == 'POST':

        if request.form['submit'] == 'Submit PNG':
            file = request.files['file']
            gender = request.form['gender']
            age = request.form['age']

            if age == "None":
                age = ""
            else:
                age = int(age)

            if file:
                name = secure_filename(file.filename)

                # convert string data to numpy array
                img = np.fromstring(file.read(), np.uint8)
                img = cv2.imdecode(img, cv2.CV_16UC1)

                # Send image to html
                file_object = io.BytesIO()
                img2 = cv2.resize(img, (500, 500))
                img2 = Image.fromarray(img.astype('uint8'))
                img2.save(file_object, 'PNG')
                img2 = base64.b64encode(file_object.getvalue())

        else:

            file = request.files['file2']

            if file:
                securedFilename = secure_filename(file.filename)

                name = securedFilename.replace(".dcm", ".png")

                ds = dicom.dcmread(file)

                if "PatientSex" in ds:
                    print("gender" + str(ds.PatientSex))
                    gender = str(ds.PatientSex)
                    if gender == "Male" or gender == "male" or gender == "M" or gender == "m":
                        gender = 'Male'
                    elif gender == "Female" or gender == "female" or gender == "F" or gender == "f":
                        gender = 'Female'
                    else:
                        gender = ''

                if "PatientAge" in ds:
                    print("age" + str(ds.PatientAge))
                    if ds.PatientAge != '':
                        age = int(ds.PatientAge)
                    else:
                        age = ""
                else:
                    age = ""

                img = ds.pixel_array

                # Send image to html
                file_object = io.BytesIO()
                # Convert tiff (I;16) to PNG
                img2 = cv2.resize(img, (500, 500))
                norm = (img2.astype(np.float) - img2.min()) * 255.0 / (img2.max() - img2.min())
                Image.fromarray(norm.astype(np.uint8)).save(file_object, "PNG")
                img2 = base64.b64encode(file_object.getvalue())
                imgData = base64.b64decode(str(img2.decode('ascii')))
                img = Image.open(io.BytesIO(imgData))

        # # Data Preparation
        # # Contrast Limit Adaptive Histogram Equalization
        imgClahe = CLAHE(np.array(img))

        # # Doing Exposure
        exposureImage = exposure(imgClahe)

        # Doing Segment
        segment = segment_lung(exposureImage)

        img = cv2.resize(segment, (672, 672))
        img = img.reshape(1, 672, 672, 1)

        segment = None
        imgClahe = None

        age, gender = dataBining(age, gender)

        contextual = (gender, age)
        contextual = np.asarray(contextual)

        nodules, nonNodules = load_Model(img, contextual)

        file=""
        return render_template("result.html", age=age, name=name, gender=gender,
                                       nodule=nodules, nonNodule=nonNodules, img=img2.decode('ascii'))



@app.route("/NoduleDetectionResult", methods=['GET', 'POST'])
def detectnodule():
    if request.method == 'POST':

        if request.form['submit'] == 'Submit Image':
            file = request.files['file2']
            name = secure_filename(file.filename)

            if ".png" in name:
                # convert string data to numpy array
                img = np.fromstring(file.read(), np.uint8)
                img = cv2.imdecode(img, cv2.CV_16UC1)

                # Send image to html
                file_object = io.BytesIO()
                originalImage = cv2.resize(img, (500, 500))
                originalImage = Image.fromarray(img.astype('uint8'))
                originalImage.save(file_object, 'PNG')
                originalImage = base64.b64encode(file_object.getvalue())

            else:
                ds = dicom.dcmread(file)
                img = ds.pixel_array
                # Send image to html
                file_object = io.BytesIO()
                # Convert tiff (I;16) to PNG
                originalImage = cv2.resize(img, (500, 500))
                norm = (originalImage.astype(np.float) - originalImage.min()) * 255.0 / (originalImage.max() - originalImage.min())
                Image.fromarray(norm.astype(np.uint8)).save(file_object, "PNG")
                originalImage = base64.b64encode(file_object.getvalue())

                imgData = base64.b64decode(str(originalImage.decode('ascii')))
                img = Image.open(io.BytesIO(imgData))

            # # Contrast Limit Adaptive Histogram Equalization
            imgClahe = CLAHE(np.array(img))

            imgClahe = cv2.merge((imgClahe, imgClahe, imgClahe))
            object_detect, count = object_Model(imgClahe)

            if count == 0:
                show = "<div class="+'"col-6">'+"<h3 style="+'"color:#293a4d; font-weight:bold; text-align: center;"'+">Nodule Detection Not Found</h3></div>"
            else:
                show = "<div class="+'"col-6">'+"<h3 style="+'"color:#293a4d; font-weight:bold; text-align: center;"'+">Nodule Detection Found: "+str(count)+"</h3></div>"

            # Send image to html
            file_object = io.BytesIO()
            object_detect = Image.fromarray(object_detect.astype('uint8'))
            object_detect.save(file_object, 'PNG')
            object_detect = base64.b64encode(file_object.getvalue())
            file=""
            return render_template("noduleDetection.html", originalImg=originalImage.decode('ascii'), detectionImg=object_detect.decode('ascii'), show = show)


# @app.route('/StartClassification')
# def send_image():
#     result = send_file(original, attachment_filename=original_name)
#     # os.remove(os.path.join("static/img/", filename))
#     return result


def dataBining(age, gender):
    # Value based on train data set
    first = 39
    second = 51
    third = 61

    if age == '':
        quatileAge = -1
    elif age < first:
        quatileAge = 1
    elif age < second:
        quatileAge = 2
    elif age < third:
        quatileAge = 3
    else:
        quatileAge = 4

    if gender == "Male":
        gender = 1
    elif gender == "Female":
        gender = 0
    else:
        gender = -1

    return quatileAge, gender


def CLAHE(img):
    clahe = cv2.createCLAHE(clipLimit=2)
    clahe_img = clahe.apply(img)
    return clahe_img


def exposure(clahe_img):
    # img = cv2.imread(clahe_img, 0)
    normalize = cv2.normalize(clahe_img.astype('float'), None, 0.0, 1.0, cv2.NORM_MINMAX)
    avg = (np.average(normalize)) * 100
    exposure_img = np.where((255 - clahe_img) < avg, 255, clahe_img + avg)
    # imageio.imwrite('static/img/exposure_img.jpg', exposure_img)
    return exposure_img


def selective_threshold(exposure_img):
    sum = 0
    T = []
    j = 0
    Threshold = 0
    hist, bins = np.histogram(exposure_img, 256, [0, 256])

    # STEP 1: Compute mean intensity of image from histogram, set T=mean(I)
    for i in range(0, len(hist)):
        sum += hist[i] * bins[i]
    T.append(sum // np.sum(hist))

    # STEP 2: Compute Mean above T(MAT) and Mean below T(MBT) using T from STEP 1
    mu2 = np.cumsum(hist[0:int(T[j])])
    first = np.sum(bins[0:int(T[j])]*hist[0:int(T[j])])/mu2[-1]
    first = round(first, 4)
    mu3 = np.cumsum(hist[int(T[j] - 1):len(hist)])
    second = np.sum((bins[int(T[j]) - 1:len(bins) - 1]) * (hist[int(T[j]) - 1:len(hist)])) / mu3[-1]
    second = round(second, 4)
    j = j + 1
    T.append(round((first + second) / 2))

    print(hist)

    # STEP 3 to n: repeat step 2 if T(i)~=T(i-1)
    while abs(int(T[j]) - int(T[j - 1])) >= 1:
        mu2 = np.cumsum(hist[0:int(T[j])])
        first = np.sum(bins[0:int(T[j])] * hist[0:int(T[j])]) / mu2[-1]
        first = round(first, 4)
        mu3 = np.cumsum(hist[int(T[j] - 1):len(hist)])
        second = np.sum((bins[int(T[j]) - 1:len(bins) - 1]) * (hist[int(T[j]) - 1:len(hist)])) / mu3[-1]
        second = round(second, 4)
        j = j + 1
        T.append(round((first + second) / 2));
        Threshold = T[j]

    print(Threshold)
    return Threshold


def bwareafilt(mask, n=1, area_range=(0, np.inf)):
    """Extract objects from binary image by size """
    # For openCV > 3.0 this can be changed to: areas_num, labels = cv2.connectedComponents(mask.astype(np.uint8))
    labels = measure.label(mask.astype('uint8'), background=0)
    area_idx = np.arange(1, np.max(labels) + 1)
    areas = np.array([np.sum(labels == i) for i in area_idx])
    inside_range_idx = np.logical_and(areas >= area_range[0], areas <= area_range[1])
    area_idx = area_idx[inside_range_idx]
    areas = areas[inside_range_idx]
    keep_idx = area_idx[np.argsort(areas)[::-1][0:n]]
    kept_areas = areas[np.argsort(areas)[::-1][0:n]]
    if np.size(kept_areas) == 0:
        kept_areas = np.array([0])
    if n == 1:
        kept_areas = kept_areas[0]
    kept_mask = np.isin(labels, keep_idx)

    return kept_mask


def segment_lung(img):
    binaryImage = img < selective_threshold(img);

    # Erosion
    binaryImage = scipy.ndimage.morphology.binary_erosion(binaryImage)
    binaryImage = scipy.ndimage.morphology.binary_erosion(binaryImage, structure=np.ones((3, 3))).astype(
        binaryImage.dtype)
    # Dilation
    binaryImage = scipy.ndimage.morphology.binary_dilation(binaryImage)
    binaryImage = scipy.ndimage.morphology.binary_dilation(binaryImage, structure=np.ones((7, 7))).astype(
        binaryImage.dtype)

    print("pass1")

    # Change edge value to be FALSE
    binaryImage[0:-1, 0:50] = False
    binaryImage[0:50:, 0:-1] = False
    binaryImage[(len(binaryImage) - 50):-1:, 0:-1] = False
    binaryImage[0:-1, len(binaryImage) - 50:-1] = False

    print("pass2")

    kept_maskLeft = bwareafilt(binaryImage, 1)
    kept_maskBoth = bwareafilt(binaryImage, 2)

    mask_maskRight = kept_maskLeft != kept_maskBoth

    # Change Binary Image to Object by Convex hull
    maskLeft = convex_hull_image(kept_maskLeft)
    maskRight = convex_hull_image(mask_maskRight)
    finalMask = maskLeft != maskRight

    print("pass3")

    # Intersect mask with the image
    segment_image = finalMask * img

    return segment_image


def load_Model(img, contextual):
    K.clear_session()
    print("pass4")
    model = load_model('static/model/colab_pro_model.h5', compile=True)
    # opt = SGD(lr=0.01)
    # print("pass5")
    # model.compile(loss=binary_crossentropy,
    #               optimizer=opt, metrics=['accuracy'])

    predict = model.predict([img.reshape(1, 672, 672, 1), contextual.reshape(1, 2)])

    model = None
    nodules = (predict[0][0] * 100)
    nonNodules = 100 - nodules

    return '{:.2f}'.format(nodules), '{:.2f}'.format(nonNodules)


class_names = ['Nodule', 'BG']


# define a configuration for the model
class NoduleConfig(Config):
    # Give the configuration a recognizable name
    NAME = "Nodule_cfg"
    # Number of classes (background + kangaroo)
    NUM_CLASSES = 1 + 1
    # Number of training steps per epoch
    STEPS_PER_EPOCH = 132


config = NoduleConfig()


class InferenceConfig(NoduleConfig):
    GPU_COUNT = 1
    IMAGES_PER_GPU = 1


inference_config = InferenceConfig()


def object_Model(img):
    print("object detection")
    K.clear_session()
    model_detection = MaskRCNN(mode='inference', model_dir='static/model', config=inference_config)
    model_detection.load_weights('static/model/mask_rcnn_nodule_new.h5',
                                 by_name=True)
    img = img
    results = model_detection.detect([img], verbose=1)
    r = results[0]

    print(r['rois'])
    object_image = display_results(img, r['rois'], r['masks'], r['class_ids'], class_names, r['scores'])

    return object_image, len(r['rois'])


def display_results(image, boxes, masks, class_ids, class_names, scores=None, show_mask=True, show_bbox=True):
    n_instances = boxes.shape[0]
    for k in range(n_instances):
        # Show box on chest X-ray image
        if show_bbox:
            box = boxes[k]
            cls = class_names[class_ids[k] - 1]  # Skip the Background
            score = scores[k]
            cv2.rectangle(image, (box[1], box[0]), (box[3], box[2]), [255,0,0], 2)
            font = cv2.FONT_HERSHEY_SIMPLEX
            cv2.putText(image, '{}: {:.3f}'.format(cls, score), (box[1], box[0]),
                        font, 0.8, (255, 0, 0), 2, cv2.LINE_AA)

        # Show mask on chest X-ray image
        if show_mask:
            mask = masks[:, :, k]
            color_mask = np.zeros((mask.shape[0], mask.shape[1], 3), dtype=np.int)
            color_mask[mask] = (0, 255, 0)
            image = cv2.addWeighted(color_mask, 0, image.astype(np.int), 1, 0)

    return image


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=8080, debug=True)


