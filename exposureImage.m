function f = exposureImage(image)
    grayImage = imread(image);
    Idouble = im2double(grayImage); 
    avg = mean2(Idouble);
    f = grayImage + (avg*100);
    path = '/Users/rosejh/PycharmProjects/SeniorProject/static/img/exposure.png';
    imwrite(f, path);
end
  
    