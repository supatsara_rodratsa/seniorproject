function f = segmentImage(image)

    grayImage = imread("/Users/rosejh/Desktop/CLAHE_new/NIH/noduleExposure/00010894_000.png");  
    % Selective Threshold Technique
    I = im2uint8(grayImage(:));
    
    % STEP 1: Compute mean intensity of image from histogram, set T=mean(I)
    [counts,N]=imhist(I);
    i=1;
    mu=cumsum(counts);
    T(i)=(sum(N.*counts))/mu(end);
    T(i)=round(T(i));
    
    % STEP 2: compute Mean above T (MAT) and Mean below T (MBT) using T from
    % step 1
    mu2=cumsum(counts(1:T(i)));
    MBT=sum(N(1:T(i)).*counts(1:T(i)))/mu2(end);
    mu3=cumsum(counts(T(i):end));
    MAT=sum(N(T(i):end).*counts(T(i):end))/mu3(end);
    i=i+1;
    
    % new T = (MAT+MBT)/2
    T(i)=round((MAT+MBT)/2);
    
    % STEP 3 to n: repeat step 2 if T(i)~=T(i-1)
    while abs(T(i)-T(i-1))>=1
        mu2=cumsum(counts(1:T(i)));
        MBT=sum(N(1:T(i)).*counts(1:T(i)))/mu2(end);

        mu3=cumsum(counts(T(i):end));
        MAT=sum(N(T(i):end).*counts(T(i):end))/mu3(end);

        i=i+1;
        T(i)=round((MAT+MBT)/2); 
        Threshold=T(i);
    end
    
    
    thresholdValue = Threshold;
    
    binaryImage = grayImage < thresholdValue;
    
    % Get rid of stuff touching the border
    binaryImage = imclearborder(binaryImage);
    % Extract only the two largest blobs.
    binaryImage = bwareafilt(binaryImage,2);
    
    binaryImage = imfill(binaryImage,'holes');
    
    % Make blobs convex and smooth.  
    % This will also fill holes in the blobs to make them solid.
    binaryImage = bwconvhull(binaryImage, 'objects');
    imshow(binaryImage,[])
    
    % Mask image with lungs-only mask.  
    % This will produce a gray scale image in the lungs and black everywhere else.
    maskedImage = grayImage; % Initialize
    maskedImage(~binaryImage) = 0;
%     f = maskedImage;
    path = '/Users/rosejh/Desktop/segment55.png';
    imwrite(maskedImage, path, 'mode', 'lossless');
%end